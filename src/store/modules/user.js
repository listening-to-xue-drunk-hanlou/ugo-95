export default {
    namespaced: true,
    state: {
      myuser: '测试vuex模块',
      token: uni.getStorageSync('ugo_token') || ''
    },
    getters: {},
    mutations: {
        updateMutaions(state, payload) {
            state.token = payload
            uni.setStorageSync('ugo_token', payload)
          }
    },
    actions: {}
  }
