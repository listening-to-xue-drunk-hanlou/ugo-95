
import Vue from 'vue'
import Vuex from 'vuex'
import user from './modules/user'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    cartList: uni.getStorageSync('cart') || []
  },
  getters: {
    isAllSelected(state) {
      // return state.cartList.every(item => item.goods_select === true)
      return state.cartList.every(item => item.goods_select)
    },
    // 先写计算属性，获取当前选中的商品
    selectGoodsList(state) {
      return state.cartList.filter(item => item.goods_select)
    },
    totalCount(state, getters) {
      // 怎么样在getters里面访问其他的getters？ 怎么样在mutations里面访问其他的mutations
      return getters.selectGoodsList.reduce((prev, item) => {
        prev+= item.goods_count
        return prev
      }, 0)
    },
    totalPrice(state, getters) {
      return getters.selectGoodsList.reduce((prev, item)=>{
        prev += Number(item.goods_count) * Number(item.goods_price)
        return prev
      }, 0)
    },
  },
  mutations: {
    addCartMutation(state, payload) {
      // state.cartList.push(payload)
      state.cartList = payload
      // uni.setStorageSync('cart', payload)
      uni.setStorage({
        key: 'cart',
        data: payload
      })
    },
    a(){
      // 如果要在一个mutations里面访问另外一个mutation。不推荐，尽量在action里面去调用
      // this.commit('addCartMutation')
    }
  },
  actions: {
    fn(context, payload) {
      context.commit('其他的mutations')
      context.commit('其他的mutations')
      context.commit('其他的mutations')
      // 调用其他的actions
      context.dispatch('其他的action')
      context.dispatch('其他的action')
      context.dispatch('其他的action')
    }
  },
  modules: {
    user
  }
})
// export default store
