import Vue from 'vue'
import App from './App'
import store from './store/index'

import uView from "uview-ui";
Vue.use(uView);

Vue.config.productionTip = false

App.mpType = 'app'
 
uni.$u.http.setConfig((config) => {
  config.baseURL = `https://api-hmugo-web.itheima.net/api/public/v1`;
  return config
})
uni.$u.http.interceptors.request.use((config) => {
	// config.header = {
	// 	...config.header,
	// 	a: 1 
	// }
	const token = store.state.user.token
	if(token && config.url.startsWith('/my')) {
		config.header = {
			...config.header,
			Authorization: token 
		}
	}
  uni.showLoading()
	return config
}, config => {
	return Promise.reject(config)
})
uni.$u.http.interceptors.response.use((response) => {
  uni.hideLoading()
	return response
}, (response) => {
  uni.hideLoading()
	return Promise.reject(response)
})

const app = new Vue({
	store,
  ...App
})
app.$mount()
